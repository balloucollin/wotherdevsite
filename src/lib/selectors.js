/**
 * Selects a single thing, wrapper for querySelector
 * @param {String} selector 
 * @returns {HTMLElement}
 */
export function $$(selector) {
    return document.querySelector(selector);
};

/**
 * 
 * @param {String} selector 
 * @returns {NodeList}
 */
export function $$$ (selector) {
    return document.querySelectorAll(selector);
};

export function $id(inputId) {
    return document.getElementById(inputId);
}