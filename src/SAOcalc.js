function calculate() {
    document.input.NetAmpLoad.value =
        1 / (1 / document.input.R3.value + 1 / document.input.HeadImp.value) +
        parseFloat(document.input.R2.value);
    
    
    document.input.NetAmpOut.value =
        1 / (1 / (parseFloat(document.input.AmpOut.value) +
            parseFloat(document.input.R2.value)) +
            1 / parseFloat(document.input.R3.value));
    
    
    document.input.Attenuation.value =
        Math.log((parseFloat(document.input.R2.value) +
            (1 / (1 / document.input.R3.value + 1 / document.input.HeadImp.value))) / (1 / (1 / document.input.R3.value + 1 / document.input.HeadImp.value))) / Math.log(10) * 20
}